Manage landscape and portrait modes with media queries
=====================================================

iPhone device sizes (from [kylejlarson.com](http://www.kylejlarson.com/blog/2015/iphone-6-screen-size-web-design-tips/)):

 * iPhone 4 - 320x480
 * iPhone 5 - 320x568
 * iPhone 6 - 375x667
 * iPhone 6 plus - 414x736

iPad device sizes, see [mydevice.io](http://mydevice.io/devices/):

* all have 768x1024


Let's assume that Android (and Windows Phone) have similar sizes for now.

```
/* portrait for all iPhones */
@media screen and (max-width: 414px)  and (orientation: portrait)  {}

/* landscape for all iPhones */
@media screen and (max-height: 414px) and (max-width: 736) and (orientation: landscape) {}

/* tablets and desktop  */
@media (min-width: 415px) and (min-height: 415px) {}
```
