Template for Cordova development
===============================

Stripped down the  `hello` app down to two files: `index.html` and `config.xml`

Development is efficient using a browser. Do `cordova platform add browser` and
then `cordova emulate browser`. Use `cordova build browser` to update the browser
when chaninging the code (do a browser refresh also).


Device pixels
-------------


iPhone 6 Plus in Portrait. Switch width/height for landscape.

```
+-------------------+---------------------+
|  availWidth:414px |                     |
|                   |                     |
|                   |                     |
|                   |                     |
|                   |                     |
|                   |                     |
| availHeight:716px |                     |
|                   |                     |
|                   |                     |
|                   |                     |
|                   |                     |
|                   |                     |
+-------------------+                     |
|                                         |
|                                         |
|                                         |
|                      innerHeight:1486px |
|                                         |
|           innerWidth:980px              |
+-----------------------------------------+
```

The number of rendered pixels on iPhone 6 Plus is 1242x2208. A splash screen
for iPhone 6 Plus should have this dimension. The number of physical pixels
differs somewhat, 1080x1920, but this is of no practical consequence.

High resolution screens (retina etc.) has a device pixel ratio higher than 1.
This

Setting the `viewport` will make availWidth/Height === innerWidth/Hieght (and )

Device Orientation
------------------

Supported device orientations is controlled in config.xml according to the
[documentation](https://cordova.apache.org/docs/en/4.0.0/config_ref_index.md.html).
This don't work on iOS but there is a [workaround](http://stackoverflow.com/questions/25054554/iphone-screen-orientation-doesnt-change-in-cordova-3-5-0). How to change supported orientations is described in
[Apple's documentation](https://developer.apple.com/library/ios/technotes/tn2244/_index.html)

NOTE: Changing the supported orientations in xcode and running from xcode works.
The settings will be overwritten if you do `cordova build` though.

The event [`deviceorientation`](http://caniuse.com/#feat=deviceorientation)

[HTML 5 Rocks](http://www.html5rocks.com/en/tutorials/device/orientation/)
article about device orientation.

Euler angles are used:

* gamma is x-axis (side-to-side) rotation
* beta is y-axis (to-to-bottom) rotation
* alpha is z-axis tilt - compass heading
