Template for Cordova development
===============================

Stripped down the  `hello` app down to two files: `index.html` and `config.xml`

Development is efficient using a browser. Do `cordova platform add browser` and
then `cordova emulate browser`. Use `cordova build browser` to update the browser
when cahninging the code (do a browser refresh also).


