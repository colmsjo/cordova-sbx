Safe Notes
===============


Pre-reqiusites
--------------

Install Xcode if you haven't already (download in App Store).

Install [Android SDK](https://developer.android.com/sdk/index.html)

 * update your path with the adt-XXX/sdk/tools directory (in `~/.bashrc` etc). This is my installation path: `export PATH=$PATH:~/local/adt-bundle-mac-x86_64-20140702/sdk/tools`

 * Run `android` and install the Android API 19 packages

Cordova: `npm install -g cordova`

Ruby needs to be installed to compile SASS. [rvm](https://rvm.io) is a good
tool to mange ruby version with.


Installation
------------

Install iOS emulator with: `npm install -g ios-sim`. Running apps on connected
devices require: `npm install -g ios-deploy`

SASS is used to generate the CSS. The SCSS toolkit [Susy](http://susy.oddbird.net)
is used to setup the grid. Widgets are from [uikit](http://getuikit.com).

* Install [SASS](http://sass-lang.com): `gem update system; gem install sass`
* Install [Susy](http://susy.oddbird.net): `gem install susy`


Setup cordova platforms and bower components with: `npm run-script setup`.
`npm run-script clean` makes cleans up everything to make it easy to setup
from scratch again.


Build and test
--------------

Commands:

 * Generate documentation: `npm run-script docs`
 * Build everything with: `npm run-script compile`
 * Build for browser only (quicker, handy when developing): `npm run-script compile-browser`
 * Emulate in browser: `npm run-script emulate-browser`
 * Emulate in iOS: `npm run-script emulate-ios`
 * Emulate in Android: `npm run-script emulate-android`
 * Run on physical iOS device: `npm run-script run-ios`
 * Run on physical Android device: `npm run-script run-android`

List available iOS platforms: `./DahliaApp1/www/platforms/ios/cordova/lib/list-emulator-images`
Emulate a specific device: `cd DahliaApp1; cordova emulate ios --target="iPhone-6-Plus"`


Responsiveness
--------------

Some notes about devices and screen sizes.

iPhone device sizes (from [kylejlarson.com](http://www.kylejlarson.com/blog/2015/iphone-6-screen-size-web-design-tips/)):

 * iPhone 4 - 320x480
 * iPhone 5 - 320x568
 * iPhone 6 - 375x667
 * iPhone 6 plus - 414x736

iPad device sizes, see [mydevice.io](http://mydevice.io/devices/):

* all have 768x1024


Let's assume that Android have similar sizes for now.

Media queries:

```
/* portrait for all iPhones */
@media screen and (max-width: 414px)  and (orientation: portrait)  {}

/* landscape for all iPhones */
@media screen and (max-height: 414px) and (max-width: 736) and (orientation: landscape) {}

/* tablets and desktop  */
@media (min-width: 415px) and (min-height: 415px) {}
```
