Cordova Sandbox
===============


Pre-reqiusites
--------------

Install Xcode if you haven't already (download in App Store).

Install [Android SDK](https://developer.android.com/sdk/index.html)

 * update your path with the adt-XXX/sdk/tools directory (in `~/.bashrc` etc). This is my installation path: `export PATH=$PATH:~/local/adt-bundle-mac-x86_64-20140702/sdk/tools`

 * Run `android` and install the Android API 19 packages

Cordova: `npm install -g cordova`


Installation
------------

Install with Apache Cordova with: `npm install -g cordova`

Create first app: `cordova create hello com.example.hello HelloWorld`

Install iOS emulator with: `npm install -g ios-sim`. Running apps on connected
devices require: `npm install -g ios-deploy`


Create an app
------------

Create first app: `cordova create hello com.organization.user.hello HelloWorld`.

It is possible to test apps in a local browser. Just do:

```
cordova platform add browser
cordova run browser
```

You need a provisioning profile matching the string `com.organization.user.*`
to be able run the app on a physical device. This is created in member central at:
[http://developer.apple.com](http://developer.apple.com). You also need to add the
device used for testing in memeber center and then download the provisioning profile
to the device. I downloaded the profile to the device using Xcode in
window->devices->settings->show povisioning profiles

[Here](https://developer.apple.com/library/ios/documentation/IDEs/Conceptual/AppDistributionGuide/Introduction/Introduction.html#//apple_ref/doc/uid/TP40012582-CH1-SW1) is an overview of the app distribution process.


I'm developing for iOS and Android:

	cd hello
	cordova platform add ios
	cordova platform add android


Build everything with: `cordova build`


Run the app
-------------

Run the app in a desktop browser first.

Browsers needs CORS disabled:
`/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --disable-web-security`

Cordova also provides a way to start the app in a browser:

```
cordova platform add browser
cordova run browser
```

Test the app:

 * Run on connected device: `cordova run ios --device`
 * Run in emulator: `cordova emulate ios`

List available iOS platforms: `./platforms/ios/cordova/lib/list-emulator-images`
Emulate a specific device: `cordova emulate ios --target="iPhone-6-Plus"`
